const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Event = require("./models/Event");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [firstUser, secondUser] = await User.create({
    email: 'admin@gmail.com',
    password: 'admin',
    displayName: 'admin',
    token: nanoid(),
    role: 'admin'
  }, {
    email: 'user@gmail.com',
    password: 'user',
    displayName: 'user',
    token: nanoid(),
    role: 'user'
  });

  await Event.create({
    title: 'My Lesson',
    datetime: '11-12-21',
    duration: '2hours',
    creator: firstUser
  }, {
    title: 'My Lesson-2',
    datetime: '12-12-21',
    duration: '5hours',
    creator: secondUser,
  })

  await mongoose.connection.close();
};

run().catch(console.error);