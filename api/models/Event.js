const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
  title : {
    type: String,
    required: true,
  },
  datetime: {
    type: String,
    required: true,
  },
  duration: {
    type: String,
    required: true,
  },
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
});

const Event = mongoose.model('Event', EventSchema);

module.exports = Event;