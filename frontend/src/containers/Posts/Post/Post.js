import React from 'react';
import {Button, Card, CardActions, Grid, Typography} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import {useDispatch, useSelector} from 'react-redux';
import useStyles from './styles';
import {FcSettings} from 'react-icons/fc';
import {eventDeleteRequest} from "../../../store/actions/eventsToolkitActions";

const Post = ({post,setCurrentId}) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const {user} = useSelector(state => state.users);

    return (
        <Card className={`${classes.card} ${classes.width}`}>
            <Grid className={classes.align}>
                <Typography variant="body2">{post.datetime}</Typography>
            </Grid>
            <Typography className={classes.title} gutterBottom variant="h5" component="h2">Title: {post.title}</Typography>
            <Grid className={classes.title}>
                <Typography variant="h6">Duration: {post.duration}</Typography>
            </Grid>
            <CardActions className={classes.cardActions}>
                {(user?._id === post?.creator) && (
                    <>
                        <Button size="small" color="primary" onClick={() => dispatch(eventDeleteRequest(post._id))}>
                            <DeleteIcon fontSize="small" /> Delete
                        </Button>
                        <Button size="small" color="primary" onClick={() => setCurrentId(post._id)}>
                            <FcSettings fontSize="small" />  Change
                        </Button>
                    </>
                )}
            </CardActions>
        </Card>
    );
};

export default Post;