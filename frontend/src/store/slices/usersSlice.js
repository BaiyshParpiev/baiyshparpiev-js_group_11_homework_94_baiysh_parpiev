import {createSlice} from '@reduxjs/toolkit';

const name = 'users';


export const initialState = {
    registerLoading: false,
    registerError: null,
    loginError: null,
    loginLoading: false,
    user: null,
};


const usersSlice = createSlice({
    name,
    initialState,
    reducers: {
        registerUserRequest(state, action) {
            state.registerLoading = true;
        },
        registerUserSuccess(state, {payload: userData}) {
            state.user = userData;
            state.registerLoading = false;
            state.registerError = null;
        },
        registerUserFailure(state, {payload: error}){
            state.registerLoading = false;
            state.registerError = error;
        },
        loginUserRequest(state, action){
            state.loginLoading = true;
        },
        loginUserSuccess(state, {payload: userData}){
            state.loginLoading = false;
            state.user = userData;
        },
        loginUserFailure(state, {payload: error}){
            state.loginLoading = false;
            state.error = error;
        },
        googleRequest(state, action){
            state.loginLoading = true;
        },
        facebookRequest(state, action){
            state.loginLoading = true;
        },
        logoutUser(state, action){
            state.user = null;
        },
        searchRequest(state, action){
            state.loginLoading = true
        },
        searchSuccess(state, {payload: user}){
            state.friend = user;
            state.loginLoading = false;
        },
        searchFailure(state, {payload: error}){
            state.loginLoading = false;
            state.error = error;
        }
    },
});

export default usersSlice;