import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
    eventsFetchRequest,
    eventsFetchSuccess,
    eventsFetchFailure,
    eventUpdateSuccess,
    eventUpdateFailure,
    eventDeleteSuccess,
    eventDeleteFailure,
    eventUpdateRequest,
    eventDeleteRequest, createEventSuccess, createEventFailure, createEventRequest
} from "../actions/eventsToolkitActions";
import {toast} from "react-toastify";

export function* fetchEvents() {
    try {
        const {data} = yield axiosApi.get('/events');
        yield put(eventsFetchSuccess(data));
    } catch (e) {
        toast.error(e.response.data.message);
        yield put(eventsFetchFailure(e.response.data));
    }
}
export function* updateFetch({payload}) {
    try {
        const {data} = yield axiosApi.put('/events/' + payload.currentId, payload.postData);
        yield put(eventUpdateSuccess(data));
    } catch (e) {
        toast.error(e.response.data.message);
        yield put(eventUpdateFailure(e.response.data));
    }
}
export function* deleteFetch({payload: id}) {
    try {
        yield axiosApi.delete('/events/' + id);
        yield put(eventDeleteSuccess(id));
    } catch (e) {
        toast.error(e.response.data.message);
        yield put(eventDeleteFailure(e.response.data));
    }
}

export function* createEvent({payload: event}) {
    try {
        console.log(event)
        const {data} = yield axiosApi.post('/events', event);
        yield put(createEventSuccess(data));
    } catch (e) {
        toast.error(e.response.data.message);
        yield put(createEventFailure(e.response.data));
    }
}



const eventsSaga = [
    takeEvery(eventsFetchRequest, fetchEvents),
    takeEvery(eventUpdateRequest, updateFetch),
    takeEvery(eventDeleteRequest, deleteFetch),
    takeEvery(createEventRequest, createEvent),
]

export default eventsSaga
