import React, {useEffect} from 'react';
import { Grid, CircularProgress } from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import Post from './Post/Post';
import useStyles from './styles';
import {eventsFetchRequest} from "../../store/actions/eventsToolkitActions";

const Posts = ({setCurrentId}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {user} = useSelector(state => state.users)
    const {posts, loading} = useSelector((state) => state.events);

    useEffect(() => {
        dispatch(eventsFetchRequest());
    }, [dispatch])

    return (
        loading ? <CircularProgress /> : (
            <Grid className={classes.mainContainer} container alignItems="stretch" spacing={3}>
                {user && posts.map((post) => (
                    <Grid key={post._id} item xs={12} sm={12} md={12}>
                        <Post post={post} setCurrentId={setCurrentId}/>
                    </Grid>
                ))}
            </Grid>
        )
    );

};

export default Posts;