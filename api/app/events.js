const express = require('express');
const Event = require('../models/Event');
const auth = require('../middleware/auth');
const router = express.Router();

router.get('/', auth,async(req, res) => {
  try{
    const events = await Event.find({creator: req.user._id}).sort({datetime: -1});
    res.send(events);
  }catch(e){
    res.sendStatus(500);
  }
});

router.post('/', auth, async(req, res) => {
  try{
    const {title, datetime, duration} = req.body;
    const event = new Event({title, datetime, duration, creator: req.user._id});
    await event.save();
    res.send(event);
  }catch{
    res.status(401).send({message: 'Data is not valid!'});
  }
});

router.put('/:id', auth, async(req, res) => {
  try{
    const { id } = req.params;
    const { title, duration, datetime, creator} = req.body;

    if(!req.user._id.equals(creator)){
      return res.status(403).send({message: 'It is not your event'})
    }
    const event = await Event.findById(id)
    if(!event) {
      return res.status(404).send(`No post with id: ${id}`);
    }
    const updatedPost = { creator, datetime, title, duration, _id: id };
    await Event.findByIdAndUpdate(id, updatedPost, { new: true });
    res.json(updatedPost);
  }catch{
    res.sendStatus(500)
  }
});

router.delete('/:id', auth, async(req, res) => {
  try{
    const {id} = req.params;
    const event = await Event.findById(id);
    console.log(event)

    if(!event) {
      return res.status(404).send(`No post with id: ${id}`);
    }
    await Event.findByIdAndRemove(id);
    res.send({message: "Post deleted successfully"});
  }catch{
    res.status(500).send({message: 'Something went wrong'})
  }
});

module.exports = router;