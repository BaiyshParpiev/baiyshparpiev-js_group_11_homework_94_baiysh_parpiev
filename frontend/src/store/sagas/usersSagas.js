import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
    registerUserRequest,
    registerUserFailure,
    registerUserSuccess,
    loginUserSuccess, loginUserRequest, loginUserFailure, facebookRequest, googleRequest, logoutUser
} from "../actions/usersToolkitActions";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";
import {setId} from "../actions/eventsToolkitActions";

export function* registerUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users', userData);
        yield put(registerUserSuccess(response.data));
        toast.success('Sign up successful')
        yield put(historyPush('/'))
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(registerUserFailure(e.response.data));
    }
}

export function* loginUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users/sessions', userData);
        yield put(loginUserSuccess(response.data.user));
        yield put(historyPush('/'));
        toast.success('Login successful');
    } catch (error) {
        yield put(loginUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

export function* facebookLogin({payload: facebookData}) {
    try {
        console.log(facebookData)
        const response = yield axiosApi.post('/users/facebookLogin', facebookData);
        yield put(loginUserSuccess(response.data.user));
        yield put(historyPush('/'));
        toast.success('Login successful')
    } catch (error) {
        yield put(loginUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

export function* googleLogin({payload: googleData}) {
    try {
        const response = yield axiosApi.post('/users/googleLogin', {
            tokenId: googleData.tokenId,
            googleId: googleData.googleId,
        });
        yield put(loginUserSuccess(response.data.user));
        yield put(historyPush('/'));
        toast.success('Login successful')
    } catch (error) {
        yield put(loginUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

export function* logoutUserSaga()  {
        yield axiosApi.delete('/users/sessions');
        yield put(historyPush('/'));
}


const usersSaga = [
    takeEvery(registerUserRequest, registerUserSaga),
    takeEvery(loginUserRequest, loginUserSaga),
    takeEvery(facebookRequest, facebookLogin),
    takeEvery(googleRequest, googleLogin),
    takeEvery(logoutUser, logoutUserSaga),
]

export default usersSaga
