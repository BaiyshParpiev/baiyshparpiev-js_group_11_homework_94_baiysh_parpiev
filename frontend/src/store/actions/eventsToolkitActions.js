import eventsSlices from "../slices/eventsSlices";

export const {
    eventsFetchRequest,
    eventsFetchSuccess,
    eventsFetchFailure,
    eventUpdateRequest,
    eventUpdateSuccess,
    eventUpdateFailure,
    eventDeleteRequest,
    eventDeleteSuccess,
    eventDeleteFailure,
    createEventRequest,
    createEventSuccess,
    createEventFailure,
} = eventsSlices.actions;