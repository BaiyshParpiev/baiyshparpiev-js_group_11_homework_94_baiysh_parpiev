import React, {useState} from 'react';
import {Link} from "react-router-dom";
import {Button, Grid, Typography, makeStyles, Grow, Container, } from "@material-ui/core";
import Posts from "../Posts/Posts";
import Form from "../../components/Form/Form";


const useStyles = makeStyles(theme => ({
    smallContainer: {
        border: '3px solid skyblue',
        margin: '10 0',
        padding: '5px 10px'
    },
    [theme.breakpoints.down('sm')]: {
        mainContainer: {
            flexDirection: 'column-reverse',
        }
    }
}))

const HomePage = () => {
    const classes = useStyles();
    const [currentId, setCurrentId] = useState(0);

    return (
            <Grow in>
                <Container>
                   <Grid container flexdirection="column">
                       <Grid className={classes.smallContainer} item container justifyContent="space-between" alignItems="center">
                           <Grid item>
                               <Typography variant="subtitle2">Event List</Typography>
                           </Grid>
                           <Grid item>
                               <Button component={Link} to="/invite" color="inherit">INVITE</Button>
                               <Button component={Link} to="/view-invite" color="inherit">View_INVITE</Button>
                           </Grid>
                       </Grid>
                       <Grid className={classes.mainContainer} container justifyContent="space-between" alignItems="stretch"
                             spacing={3}>
                           <Grid item xs={12} md={7}>
                               <Posts setCurrentId={setCurrentId}/>
                           </Grid>
                           <Grid item xs={12} sm={7} md={4}>
                               <Form currentId={currentId} setCurrentId={setCurrentId}/>
                           </Grid>
                       </Grid>
                   </Grid>
                </Container>
            </Grow>
    );
};

export default HomePage;