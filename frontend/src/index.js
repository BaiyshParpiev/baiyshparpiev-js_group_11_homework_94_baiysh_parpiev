import React from 'react';
import ReactDOM from 'react-dom';
import {Router} from 'react-router-dom';
import {Provider} from "react-redux";
import App from './App';
import theme from "./theme";
import history from "./history";
import 'react-toastify/dist/ReactToastify.css'
import {ToastContainer} from "react-toastify";
import {ThemeProvider} from "@material-ui/core";
import store from "./store/configStoreToolkit";




const app = (
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <Router history={history}>
                <ToastContainer/>
                    <App/>
            </Router>
        </ThemeProvider>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));

