import {createSlice} from '@reduxjs/toolkit';

const name = 'events';


export const initialState = {
    loading: false,
    error: null,
    posts: [],
};


const eventsSlice = createSlice({
    name,
    initialState,
    reducers: {
        eventsFetchRequest(state, action) {
            state.loading = true;
        },
        eventsFetchSuccess(state, {payload: events}) {
            state.posts = events;
            state.loading = false;
            state.error = null;
        },
        eventsFetchFailure(state, {payload: error}) {
            state.loading = false;
            state.error = error;
        },
        eventDeleteRequest(state, action) {
            state.loading = true;
        },
        createEventRequest(state, action){
            state.loading = true;
        },
        createEventSuccess(state, {payload: event}){
            state.loading = false;
            state.posts = [...state.posts, event]
            state.error = null;
        },
        createEventFailure(state, {payload: error}){
          state.loading = false;
          state.error = error;
        },
        eventDeleteSuccess(state, {payload: id}) {
            state.posts = state.posts.filter(p => p._id !== id);
            state.loading = false;
        },
        eventDeleteFailure(state, {payload: error}) {
            state.loading = false;
            state.error = error;
        },
        eventUpdateRequest(state, action){
          state.loading = true;
        },
        eventUpdateSuccess(state, {payload: data}){
            state.loading = false;
            state.posts = state.posts.map(post => post._id === data._id ? data : post)
        },
        eventUpdateFailure(state, {payload: error}){
            state.loading = false;
            state.error = error;
        },

    },
});

export default eventsSlice;