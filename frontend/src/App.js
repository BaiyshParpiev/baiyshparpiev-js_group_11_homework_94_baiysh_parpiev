import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import HomePage from "./containers/HomePage/HomePage";
import AddNew from "./components/AddNew/AddNew";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <Route path="/register" component={Register}/>
                <ProtectedRoute
                    path="/"
                    component={HomePage}
                    isAllowed={user}
                    redirectTo="/register"
                    exact
                />
                <ProtectedRoute
                    path="/"
                    component={AddNew}
                    isAllowed={user}
                    redirectTo="/invite"
                    exact
                />
                <Route path="/login" component={Login}/>
                <Route path="/invite" component={AddNew}/>
            </Switch>
        </Layout>
    );
};

export default App;
