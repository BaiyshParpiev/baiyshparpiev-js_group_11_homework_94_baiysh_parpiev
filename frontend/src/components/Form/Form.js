import React, {useEffect, useState} from 'react';
import useStyles from './styles';
import {TextField, Button, Typography, Paper} from '@material-ui/core';
import {useDispatch, useSelector} from "react-redux";
import {createEventRequest, eventUpdateRequest} from "../../store/actions/eventsToolkitActions";

const Form = ({setCurrentId, currentId}) => {
    const d = new Date();
    const year = (d.getFullYear()).toString();
    const month = ((d.getMonth()) + 101).toString().slice(-2);
    const date = ((d.getDate()) + 100).toString().slice(-2);

    const hours = ((d.getHours()) + 100).toString().slice(-2);
    const mins = ((d.getMinutes()) + 100).toString().slice(-2);

    const datenow = `${year}-${month}-${date}T${hours}:${mins}`
    const classes = useStyles();
    const dispatch = useDispatch();
    const {user} = useSelector(state => state.users);
    const post = useSelector(state => currentId ? state.events.posts.find(p => p._id === currentId) : null);


    const [postData, setPostData] = useState({
        title: "",
        duration: "",
        datetime: datenow,
    });

    useEffect(() => {
        if(post){
            setPostData(post);
        }
    }, [post])


    const clear = () => {
        setCurrentId(0)
        setPostData({
            title: "",
            duration: "",
            datetime: datenow,
        })
    };

    const handleSubmit = e => {
        e.preventDefault();
        if(currentId === 0){
            dispatch(createEventRequest({...postData}));
        }else{
            dispatch(eventUpdateRequest( {currentId, postData}));
        }

        clear();
    }



    if(!user?.displayName){
        return (
            <Paper className={classes.paper}>
                <Typography variant="h6" alignitems="center">
                    Please Sign In to create your own event
                    list or news and invite another.
                </Typography>
            </Paper>
        )
    }
    return (
        <Paper className={classes.paper}>
            <form autoComplete="off" noValidate className={`${classes.root} ${classes.form}`} onSubmit={e => handleSubmit(e)}>
                <Typography variant='h6'>{ currentId === 0?  'Editing': ' Creating'} an Event</Typography>
                <TextField
                    required
                    name="title"
                    variant="outlined"
                    label="Title"
                    fullWidth
                    value={postData.title}
                    onChange={(e) => setPostData({...postData, title: e.target.value})}
                /><TextField
                    name="duration"
                    variant="outlined"
                    label="Duration"
                    fullWidth
                    value={postData.duration}
                    onChange={(e) => setPostData({...postData, duration: e.target.value})}
                />
                <TextField
                    onChange={(e) => setPostData({...postData, datetime: e.target.value})}
                    name="datetime"
                    variant="outlined"
                    fullWidth
                    id="datetime-local"
                    label="Datetime"
                    type="datetime-local"
                    value={datenow}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <Button className={classes.buttonSubmit} variant="contained" color="primary" size="large" type="submit" fullWidth >Create</Button>
                <Button variant="contained" color="secondary" size="small" onClick={clear} fullWidth>Clear</Button>
            </form>
        </Paper>
    );
};

export default Form;