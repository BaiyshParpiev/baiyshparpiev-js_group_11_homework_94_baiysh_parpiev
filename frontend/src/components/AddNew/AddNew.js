import React, {useState} from 'react';
import {Button, Grid, makeStyles, Paper, TextField, Typography} from '@material-ui/core';
import {useDispatch, useSelector} from "react-redux";

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
        },
    },
    paper: {
        padding: theme.spacing(2),
    },
    form: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    fileInput: {
        width: '97%',
        margin: '10px 0',
    },
    buttonSubmit: {
        marginBottom: 10,
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
}));

const AddNew = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {user} = useSelector(state => state.users);

    const [name, setName] = useState('');

    const clear = () => {
        setName('')
    };

    const handleSubmit = e => {
        e.preventDefault();
    }

    return (
        <Paper className={classes.paper}>
            <form autoComplete="off" noValidate className={`${classes.root} ${classes.form}`} onSubmit={e => handleSubmit(e)}>
                <TextField
                    required
                    name="name"
                    variant="outlined"
                    label="Name of your friend"
                    fullWidth
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
                <Button className={classes.buttonSubmit} variant="contained" color="primary" size="large" type="submit" fullWidth >Search</Button>
                <Button variant="contained" color="secondary" size="small" onClick={clear} fullWidth>Clear</Button>
            </form>
            <Grid><Typography variant="subtitle2">{}</Typography></Grid>
        </Paper>
    );
};

export default AddNew;