import usersSlice from "../slices/usersSlice";

export const {
    registerUserRequest,
    registerUserSuccess,
    registerUserFailure,
    loginUserRequest,
    loginUserSuccess,
    loginUserFailure,
    facebookRequest,
    googleRequest,
    logoutUser,
    searchRequest,
    searchSuccess,
    searchFailure
} = usersSlice.actions;